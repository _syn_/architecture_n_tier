# -*- coding: utf-8 -*-
"""
Created on Mon Mar 13 15:14:29 2023

@author: admin
"""
from abc import ABC, abstractmethod
import pickle
import jsonpickle

import sqlalchemy
from Store import Store
from typing import List
from typing import Optional
from sqlalchemy import ForeignKey, Column, Integer, create_engine
from sqlalchemy import String
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship


class Persist(ABC):
	def __init__(self) -> None:
		pass

	@abstractmethod
	def save(self, menu):
		pass

	@abstractmethod
	def load(self):
		pass

class PersistBinary(Persist):
	def __init__(self) -> None:
		super().__init__()
		self._gagfile = './gag.bjson'

	def save(self, menu):
		with open(self._gagfile, mode='wb') as bin_file:
			pickle.dump(menu, bin_file)

	def load(self):
		with open(self._gagfile, mode='wb') as bin_file:
			menu = pickle.load(bin_file)
		return menu

class PersistJson(Persist):
    def __init__(self) -> None:
        super().__init__()
        self._gagfile = './gag.json'

    def save(self, application):
        cursor = self._connection.cursor()
        self._init_tables(cursor)
        self._save_stores(cursor, application)
        self._save_users_libraries(cursor, application)
        self._save_games(cursor, application)
        self._save_games_in_lib(cursor, application)
        # cursor.execute(f'UPDATE gag_stores SET name ')

    def _init_tables(self, cursor):
        engine = create_engine("mysql+pymysql://root:localhost@/bdd_archi_n_tiers") # bdd vide sur phpmyadmin
        metadata = sqlalchemy.MetaData()
        stores_table = sqlalchemy.Table('gag_stores',metadata, Column('id',Integer,primary_key=True,autoincrement=True),Column('name', String))
        libraries_table = sqlalchemy.Table('gag_libraries',metadata, Column('id',Integer,primary_key=True,autoincrement=True),Column('name',String))
        users_table = sqlalchemy.Table('gag_users',metadata,Column('id',Integer,primary_key=True,autoincrement=True),Column('name',String))
        games_table = sqlalchemy.Table('gag_games',metadata,Column('id',Integer,primary_key=True,autoincrement=True),Column('name',String),Column('tag',String),Column('image',String),Column('price',Integer))
        comment_table = sqlalchemy.Table('gag_comment',metadata,Column('id',Integer,primary_key=True,autoincrement=True),Column('Grade',Integer),Column('Content',String))
        metadata.create_all(engine)

    def load(self):
        application = None
        try:
            with open(self._gagfile, 'r') as json_file:
                application_str = json_file.read()
                application = jsonpickle.decode(application_str)
        except:
            print('Unable to load file')
        return application
