# -*- coding: utf-8 -*-
"""
Created on Mon Mar 13 13:29:21 2023

@author: admin
"""
from game import Game


class Store:
    def __init__(self):
        self._games = set()

    def createGame(self, name, tag, image, price):
        game = Game(name, tag, image, price)
        self._games.add(game)

    def buyGame(self, name, user):
        for game in self._games:
            if game._name == name:
                user.buyGame(game)
    
    def removeGame(self, game):
        self._games.discard(game)
    
    def __str__(self):
        result = '\n'
        for game in self._games:
            result += str(game) + '\n'
        return result