# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#---------------------main--------------------------
from store import Store
from user import User
from menu import Menu
from persist import PersistJson
from application import Application
from server import Server,ServerHttp

if __name__ == '__main__':
    persist = PersistJson()
    application = persist.load()
    if application == None:
        user = User(name='user1')
        store = Store()
        application = Application(store, user, persist)
    menu = Menu(application)
    #server = Server(menu)
    server = ServerHttp(menu)
    server.run()
