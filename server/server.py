# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 08:47:33 2022

@author: syn
mail: mathisjoly6@gmail.com
Python3.5.3

"""
#import socket
#import os
#from _thread import *

#class server(object):
    
#    def __init__(self,ip,port):
#        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#        self.socket.bind((ip, port))          #socket.bind(('10.3.141.1', 8888))

#    def server_run(self):
#        while True:
#                self.socket.listen(5)
#                client, address = self.socket.accept()
#                print ("{} connected".format( address ))

#                response = client.recv(255)
#                if response != "":
#                        print("{}".format(address))
#                        print (str(response))
#                        return(response)
#        print ("Close")
#        client.close()
#        stock.close()


import socket
import pickle
import jsonpickle
import time
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs

#HOST = 'localhost'  # Symbolic name meaning all available interfaces
#PORT = 12345  # Arbitrary non-privileged port


HOST = 'localhost'
PORT = 8000

class Server:
    def __init__(self, menu) -> None:
        self._menu = menu

    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((HOST, PORT))
            s.listen(1)
            print(f'Server listening on port {PORT}...')
            while True:
                conn, addr = s.accept()
                with conn:
                    print(f'Connected by {addr}')
                    while True:
                        time.sleep(10)
                        data = conn.recv(1024)
                        if not data:
                            break
                        parameters = pickle.loads(data)
                        result = self._menu._main_entries[parameters['action']](parameters)
                        self._menu._application.save()
                        conn.sendall(result.encode())


def create_handler(menu):
    class GagHTTPHandler(BaseHTTPRequestHandler):
        def __init__(self, *args, **kwargs) -> None:
            print('>>>>>>>>>init gag http handler')
            self._menu = menu
            super().__init__(*args, **kwargs)

        def do_GET(self):
            parsed_path = urlparse(self.path)
            path = parsed_path.path
            query = parse_qs(parsed_path.query)
            response_messsage = ''
            root_path = {

                '/lib/games': self._menu.displayGames,
                '/store/games': self._menu.displayGamesInStore,
                '/store/game/buy': self._menu.buyGame,
                '/store/game': self._menu.detailGame
            }
            parameters = {}
            print(query)
            if 'game' in query:
                game_name = query['game'][0]
                parameters['name'] = game_name
            response_messsage = root_path[path](parameters)

        def do_POST(self):
            parsed_path = urlparse(self.path)
            path = parsed_path.path
            length = int(self.headers['Content-Length'])
            field_data = self.rfile.read(length)
            response_message = 'nothing'
            if self.path == '/store/game':
                game = jsonpickle.decode(field_data)
                pass
            if self.path == '/lib/game/comment':
                pass
            print(self.request)

    return GagHTTPHandler



class ServerHttp:
    def init(self, menu) -> None:
        self._menu = menu

    def run(self, server_class=HTTPServer):
        server_address = ('', PORT)
        httpd = server_class(server_address,create_handler(self._menu))
        httpd.serve_forever()