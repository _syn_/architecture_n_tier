# -*- coding: utf-8 -*-
"""
Created on Wed May  4 09:48:16 2022

@author: syn
mail: mathisjoly6@gmail.com
Python3.5.3
"""

#import os
#import socket
#from socket import*
#import time



#class Client:
#
#    def __init__(self,host,port):
#        self.host = host                                   #   "10.3.141.1"
#        self.port = port                                   #   8888
#
#    def send_socket(self,data):
#        import socket
#        socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#        socket.connect((self.host,self.port))
#        print("connection on {}".format(self.port))
#        socket.send(str.encode(data,"utf-8"))
#        print("Close")
#        socket.close()


import socket
import pickle
import requests

HOST = 'localhost'  # The server's hostname or IP address
PORT = 12345        # The port used by the server

class Client:
    def __init__(self) -> None:
        pass

    def send(self, action, parameters):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            parameters['action'] = action
            data = pickle.dumps(parameters)
            s.sendall(data)
            print(s.getsockname())
            data = s.recv(1024)
        print(f'Received: {data.decode()}')


class ClientHTTP:
    def __init__(self, server, port) -> None:
        self._server = server
        self._port = port

    def send(self, action, parameters):
        url = f'http://{self._server}:{self._port}/{action}'
        payload = {}
        headers = {}

        response = requests.request("GET", url, headers=headers, data=payload)
        return response.content.decode('UTF-8')
